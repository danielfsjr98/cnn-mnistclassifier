package org.mnistCNN;

import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.earlystopping.EarlyStoppingConfiguration;
import org.deeplearning4j.earlystopping.EarlyStoppingResult;
import org.deeplearning4j.earlystopping.saver.LocalFileModelSaver;
import org.deeplearning4j.earlystopping.scorecalc.DataSetLossCalculator;
import org.deeplearning4j.earlystopping.termination.MaxEpochsTerminationCondition;
import org.deeplearning4j.earlystopping.termination.MaxTimeIterationTerminationCondition;
import org.deeplearning4j.earlystopping.termination.ScoreImprovementEpochTerminationCondition;
import org.deeplearning4j.earlystopping.trainer.EarlyStoppingTrainer;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws IOException {
        int seed = 12345;
        int numClasses = 10; // pois output é de 0 a 9
        int batchSize = 64;

        // Configuração da rede neural
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .l2(0.0005)
                .updater(new Nesterovs(0.01, 0.9))
                .list()
                .layer(
                        new ConvolutionLayer.Builder(5, 5)
                        .nIn(1)
                        .stride(1, 1)
                        .nOut(20)
                        .activation(Activation.IDENTITY)
                        .build()
                )
                .layer(
                        new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(2, 2)
                        .stride(2, 2)
                        .build()
                )
                .layer(
                        new ConvolutionLayer.Builder(5, 5)
                        .stride(1, 1)
                        .nOut(50)
                        .activation(Activation.IDENTITY)
                        .build()
                )
                .layer(
                        new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(2, 2)
                        .stride(2, 2)
                        .build()
                )
                .layer(
                        new DenseLayer.Builder().activation(Activation.RELU)
                        .nOut(500).build()
                )
                .layer(
                        new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(numClasses)
                        .activation(Activation.SOFTMAX)
                        .build()
                )
                .setInputType(InputType.convolutionalFlat(28, 28, 1))
                .build();

        // Carregar e pré-processar os dados de treinamento
        DataSetIterator mnistTrain = new MnistDataSetIterator(batchSize, true, seed);
        visualizeImages(mnistTrain);

        // Treinamento da rede neural
        EarlyStoppingConfiguration<MultiLayerNetwork> esConf = new EarlyStoppingConfiguration.Builder<MultiLayerNetwork>()
                .epochTerminationConditions(
                        new MaxEpochsTerminationCondition(20),
                        new ScoreImprovementEpochTerminationCondition(3, 0.001)
                )
                .iterationTerminationConditions(new MaxTimeIterationTerminationCondition(20, TimeUnit.MINUTES))
                .scoreCalculator(new DataSetLossCalculator(mnistTrain, true))
                .evaluateEveryNEpochs(1)
                .modelSaver(new LocalFileModelSaver("src"))
                .build();

        EarlyStoppingTrainer trainer = new EarlyStoppingTrainer(esConf, conf, mnistTrain);

        // Conduct early stopping training:
        EarlyStoppingResult<MultiLayerNetwork> result = trainer.fit();

        // Print out the results:
        System.out.println("Termination reason: " + result.getTerminationReason());
        System.out.println("Termination details: " + result.getTerminationDetails());
        System.out.println("Total epochs: " + result.getTotalEpochs());
        System.out.println("Best epoch number: " + result.getBestModelEpoch());
        System.out.println("Score at best epoch: " + result.getBestModelScore());

        DataSetIterator mnistTest = new MnistDataSetIterator(batchSize, false, seed);
        visualizeImages(mnistTest);
        // Avaliação do modelo
        // Obter o melhor modelo do EarlyStoppingResult
        MultiLayerNetwork bestModel = result.getBestModel();
        // MultiLayerNetwork bestModel = getBestModel(); // Utilizar melhor modelo presente no arquivo bestModel.bin

        // Avaliar o modelo nos dados de teste
        Evaluation eval = bestModel.evaluate(mnistTest);
        System.out.println(eval.stats());
    }

    public static void visualizeImages(DataSetIterator mnist) {

        List<INDArray> images = new ArrayList<>();
        int numImagesToShow = 50;
        int count = 0;

        while (mnist.hasNext() && count < numImagesToShow) {
            DataSet dataSet = mnist.next();
            INDArray features = dataSet.getFeatures();
            images.add(features);
            count++;
        }

        int numImages = images.size();
        int numRows = (int) Math.ceil(Math.sqrt(numImages));
        int numCols = numRows;

        JFrame frame = new JFrame("MNIST Images");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(numRows, numCols));

        for (int i = 0; i < numImages; i++) {
            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());

            INDArray image = images.get(i);
            Image img = createImage(image);

            JLabel label = new JLabel(new ImageIcon(img));
            panel.add(label, BorderLayout.CENTER);

            frame.add(panel);
        }

        frame.pack();
        frame.setVisible(true);
    }

    public static Image createImage(INDArray image) {
        int height = 28;
        int width = 28;
        byte[] pixels = new byte[height * width];

        for (int i = 0; i < height * width; i++) {
            int pixelValue = (int) (image.getDouble(i) * 255); // Escalando o valor do pixel para 0-255
            byte grayscaleValue = (byte) pixelValue; // Convertendo para byte

            pixels[i] = grayscaleValue;
        }

        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
        bufferedImage.getRaster().setDataElements(0, 0, width, height, pixels);

        return bufferedImage;
    }

    public static void printBestModel() {
        // Carregar o modelo salvo
        MultiLayerNetwork loadedModel;
        try {
            loadedModel = ModelSerializer.restoreMultiLayerNetwork("src/bestModel.bin");
            System.out.println(loadedModel.summary());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static MultiLayerNetwork getBestModel() {
        // Carregar o modelo salvo
        MultiLayerNetwork loadedModel = null;
        try {
            loadedModel = ModelSerializer.restoreMultiLayerNetwork("src/bestModel.bin");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return loadedModel;
    }
}